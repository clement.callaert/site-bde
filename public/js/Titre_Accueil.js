let button_element = document.querySelector(".btn");
let navElement = document.querySelector("#nav_id");
let listElements = document.querySelectorAll("#nav_id ul li");

let isNavVisible = false;

function toggleNav() {
    isNavVisible = !isNavVisible;
    if (isNavVisible) {
        for (let i = 0; i < listElements.length; i++) {
            listElements[i].style.marginRight = "2rem";
        }
    } else {
        for (let i = 0; i < listElements.length; i++) {
            listElements[i].style.marginRight = "-20rem";
        }
    }
}

button_element.addEventListener('click', toggleNav);
