const express = require('express');

const bcrypt = require('bcryptjs');

const db = require('../data/database');
const Post = require('../models/post')

const router = express.Router();

router.get('/', function (req, res) {
  res.render('index');
});

router.get('/profile', function(req, res) {
  if (!req.session.isAuthenticated) {
    return res.status(401).render('401');
  }
  res.render('profile');
});

router.get('/admin', async function (req, res) {
  if (!req.session.isAuthenticated) {
    return res.status(401).render('401');
  }
  const user = await db.getDb().collection('users').findOne({_id: req.session.user.id})
  if (!user || !user.isAdmin) {
    return res.status(403).render('403');
  }

  const users = await db.getDb().collection('users').find().toArray();
  const postData = await db.getDb().collection('posts').find().toArray();
  const resData = await db.getDb().collection('res').find().toArray();
  res.render('adminPage', {users: users, postData: postData, resData:resData});
})


router.get('/contact', function (req, res) {
  res.render('futur')
})

router.get('/sports', function (req,res) {
  res.render('futur')
})

router.get('/services', function (req,res) {
  res.render('futur')
})

router.get('/team', function (req,res) {
  res.render('futur')
})

router.get('/a-propos', function (req,res) {
  res.render('futur')
})



module.exports = router;
