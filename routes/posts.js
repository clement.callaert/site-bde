const express = require('express');
const mongodb = require('mongodb')
const bcrypt = require('bcryptjs');
const multer = require('multer')

const storageConfig = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'images');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname);
  }
});

const upload = multer({storage: storageConfig});

const db = require('../data/database');

const router = express.Router();


router.post('/creerPost',  async function (req,res) {
  const postData = req.body;
  const enteredTitre = postData.titre;
  const enteredCommentaire = postData.commentairePost;
  const enterdRecompense = postData.recompense;


  const post ={
    titre: enteredTitre,
    commentaire: enteredCommentaire,
    recompense: enterdRecompense,
    isFinish: false,
  }
  await db.getDb().collection('posts').insertOne(post);
  return res.redirect('/admin');
})

router.get('/hotLine', async function (req,res) {
  if (!req.session.isAuthenticated) {
    return res.status(401).render('401');
  }
  const postData = await db.getDb().collection('posts').find().toArray();
  const commandeData = await db.getDb().collection('commandes').find().toArray();
  res.render('hotLine', {postData: postData, commandeData: commandeData})
})

router.post('/commandeCrepe', async function (req, res) {
  const crepeData = req.body;
  const enteredCommentaire = crepeData.commentaire;
  const enteredGarniture = crepeData.garniture;
  const nom = req.session.user.nom;
  const prenom = req.session.user.prenom;



  const crepeCommande = {
    garniture: enteredGarniture,
    commentaire: enteredCommentaire,
    nom: nom,
    prenom: prenom,
    finish: false
  }
  await db.getDb().collection('commandes').insertOne(crepeCommande);
  res.redirect("/hotLine");
})

router.post('/submitResolution/:postId', upload.single('image'), async function (req, res) {
  const postId = req.params.postId; // Assuming postId is obtained from the request
  const resolutionData = req.body;
  const enteredDescription = resolutionData.description;
  const user = req.session.user;
  const nom = user.nom;
  const prenom = user.prenom;
  const file = req.file;
  const path = file.path;

  const resolution = {
      postId: postId,
      description: enteredDescription,
      nom: nom,
      prenom: prenom,
      imagePath: path
    }
  const result = await db.getDb().collection('res').insertOne(resolution);
  return res.redirect('/hotLine')
})

router.post('/accepterPost/:id', async function (req, res) {
  const postId = req.params.id;
  const winner = req.body.utilisateur;

  const ObjectID = mongodb.ObjectId;
  const postObjectId = new ObjectID(postId);

  await db.getDb().collection('posts').updateOne({ _id: postObjectId }, { $set: { isFinish: true, winner: winner } });

  await db.getDb().collection('res').updateOne({ _id: postObjectId }, { $set: { winner: winner } });

  return res.redirect('/admin');
});


router.post('/supprimerPost/:id', async function (req, res) {
  const postId = req.params.id;
  const ObjectID = mongodb.ObjectId;
  const postObjectId = new ObjectID(postId);
  await db.getDb().collection('posts').deleteOne({ _id: postObjectId });
  return res.redirect('/admin');
});


module.exports = router;